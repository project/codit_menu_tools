<?php

namespace Drupal\codit_menu_tools;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;
use Drupal\system\Entity\Menu;

/**
 * Base class for Menu manipulation.
 */
abstract class MenuManipulatorBase implements MenuManipulatorBaseInterface {

  /**
   * The logger for the module.
   *
   * @var \Drupal\monolog\Logger\Logger
   */
  public $logger;

  /**
   * The machine name of the menu.
   *
   * @var string
   */
  protected $menuName;

  /**
   * The menu lin manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuManager;

  /**
   * The menu storage service.
   *
   * @var \Drupal\menu_link_content\MenuLinkContentStorageInterface
   */
  protected $menuStorage;

  /**
   * The MenuLinkTreeInterface.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * The loaded menu tree elements.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeElement[]
   */
  protected $menuTree;

  /**
   * Constructor for MenuManipulatorBase.
   *
   * @param string $menu_name
   *   The name of the menu to load.
   */
  public function __construct(string $menu_name) {
    // Can't use it with Dependency Injection because runs from script or
    // hook_update_N where DI is not available.
    $this->menuStorage = \Drupal::entityTypeManager()->getStorage('menu_link_content');
    $this->menuManager = \Drupal::service('plugin.manager.menu.link');
    $this->menuLinkTree = \Drupal::menuTree();
    $this->logger = \Drupal::logger('codit_menu_tools');
    $this->getMenuTree($menu_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getMenuTree(string $menu_name = '') {
    if (empty($menu_name) && empty($this->menuName)) {
      throw new \Exception('MenuManipulatorBase::getMenuTree must have a menu name to load.');
    }
    if (empty($this->menuTree)) {
      // Try to load the menu since it is not available.
      if (!empty($menu_name)) {
        $this->menuName = $menu_name;
      }

      $tree = $this->menuLinkTree->load($this->menuName, new MenuTreeParameters());
      if (empty($tree)) {
        throw new \Exception("The Menu manipulator found no menu by the name '$menu_name'.");
      }
      $this->menuTree = $tree;
    }

    return $this->menuTree;
  }

  /**
   * Validate that the title and destination are provided.
   *
   * @param string $item_title
   *   The title of the intended menu item.
   * @param string $item_destination
   *   The destination of the intended menu item.
   *
   * @throws \Exception
   *   If any of the required menu details are not provided.
   */
  protected function preCheckRequiredItemValues(string $item_title, string $item_destination) : void {
    // Should throw exception if any of these values are empty.
    if (empty($item_title) && empty($item_destination)) {
      throw new \Exception("In order for MenuManipulatorBase to add a menu item, it must have a title and destination.");
    }
    elseif (empty($item_title)) {
      throw new \Exception("In order for MenuManipulatorBase to add a menu item, it must have a title.");
    }
    elseif (empty($item_destination)) {
      throw new \Exception("In order for MenuManipulatorBase to add a menu item, it must have a destination.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadMenuItemByNameAndParentName(string $menu_item_title = '', string $parent_title = ''): MenuLinkContent|null {
    $parent_id = '';
    if (!empty($parent_title)) {
      $parent_details = $this->findMenuItemDetailsByName($parent_title);
      $parent_id = $parent_details['id'] ?? '';
    }
    return $this->loadMenuItemByNameAndParentId($menu_item_title, $parent_id);
  }

  /**
   * {@inheritdoc}
   */
  public function loadMenuItemByNameAndParentId(string $menu_item_title = '', $parent_id = ''): MenuLinkContent|null {
    if (!empty($menu_item_title)) {
      $menu_tree_parameters = new MenuTreeParameters();
      $menu_tree_parameters->addCondition('title', $menu_item_title, '=');
      if (!empty($parent_id)) {
        $menu_tree_parameters->addCondition('parent', $parent_id, '=');
      }
      $tree = $this->menuLinkTree->load($this->menuName, $menu_tree_parameters);
      if (empty($tree)) {
        $vars = [
          '@title' => $menu_item_title,
          '@menu' => $this->menuName,
          ':link' => "/admin/structure/menu/manage/{$this->menuName}",
        ];
        // This menu item was not found.  Not worth bailing
        // out, because we do not know the intent. So just log it.
        $message = "Menu @menu; '@title' menu item not found. </br>";
        $message .= "Check menu <a href=\":link\">@menu</a> to see if title we were looking for '@title' has been deleted or named something else.";
        $this->logger->notice($message, $vars);
        return NULL;
      }

      if (count($tree) > 1) {
        $vars = [
          '@title' => $menu_item_title,
          '@menu' => $this->menuName,
          '@instances' => count($tree),
          ':link' => "/admin/structure/menu/manage/{$this->menuName}",
        ];
        // There are multiple menu elements with this title.  Not worth bailing
        // out, but we should log it so human overlords can look into it.
        $message = "Menu @menu; @instances '@title' menu items found. </br>";
        $message .= "Operated only on the last of the @instances instances. </br>";
        $message .= "Check menu <a href=\":link\">@menu</a> to see if the operation involving menu items '@title' was correct.";
        $this->logger->notice($message, $vars);
      }

      // Grab the last instance in case multiple exist.
      // This is a little risky, but it is an edge case.
      $menuItem = end($tree);
      return $menuItem->link;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function findMenuItemDetailsByName(string $menu_item_title = '', $parent_id = ''): array {
    $item_details = [];
    $menuItem = $this->loadMenuItemByNameAndParentId($menu_item_title, $parent_id);
    if ($menuItem) {
      $item_details['mid'] = $menuItem->getEntity()->id();
      $item_details['id'] = $menuItem->getPluginId();
      // Send back a 0 for weight as a neutral failsafe if none is recorded.
      $item_details['weight'] = (empty($menuItem->getWeight())) ? 0 : $menuItem->getWeight();
      // Use the actual parent id rather than what was passed in.
      // What was passed in was just to make the search more precise.
      $item_details['parent_id'] = $menuItem->getParent();
    }
    return $item_details;
  }

  /**
   * Re-weight all menu items in a menutree to create space to insert.
   *
   * @param array $menu_items
   *   An array of menu items from a tree trunk or subtree.
   */
  protected function separateTreeBranch(array $menu_items): int {
    $sortable = [];
    $highwater_mark = 0;
    foreach ($menu_items as $plugin_id => $menu_item) {
      $original_weight = $menu_item->link->getWeight();
      $menu_entity = $menu_item->link->getEntity();
      $id = $menu_entity->id();
      $sortable[$plugin_id] = [
        'id' => $id,
        'menu' => $menu_entity,
        'weight' => $original_weight,
      ];
      if (!empty($menu_item->subtree)) {
        // There are subtrees that need separating.
        $water_line = $this->separateTreeBranch($menu_item->subtree);
        $highwater_mark = ($water_line > $highwater_mark) ? $water_line : $highwater_mark;
      }
    }
    uasort($sortable, self::mimicDrupalMenuSort(...));
    $i = 0;
    foreach ($sortable as $plugin_id => $details) {
      $new_weight = ++$i * 2;
      if ($original_weight !== $new_weight) {
        $definition = $details['menu']->getPluginDefinition();
        $definition['weight'] = $new_weight;
        $this->menuManager->updateDefinition($plugin_id, $definition);
      }

      $highwater_mark = ($new_weight > $highwater_mark) ? $new_weight : $highwater_mark;
    }
    return $highwater_mark;
  }

  /**
   * A sort calback to mimic how Drupal sorts by weight, then menu id.
   *
   * @param array $a
   *   An array of id and weight for the first item in the sort.
   * @param array $b
   *   An array of id and weight for the second item in the sort.
   *
   * @return int
   *   An integer indicating whether a should go above or below b.
   */
  private static function mimicDrupalMenuSort($a, $b): int {
    if ($a['weight'] === $b['weight']) {
      // We have to sort by id.
      return ($a['id'] < $b['id']) ? -1 : 1;
    }
    else {
      // Sort by weight.
      return ($a['weight'] < $b['weight']) ? -1 : 1;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getAllMenuNames($machine_name_partial = NULL): array {
    // Load all the menus.
    $menus = Menu::loadMultiple();
    $menu_matches = [];
    // Only return machine_name => label. We don't need the entire menu entity.
    foreach ($menus as $menu_name => $menu) {
      if ($machine_name_partial) {
        if (str_contains($menu_name, $machine_name_partial)) {
          $menu_matches[$menu_name] = $menu->label();
        }
      }
      else {
        $menu_matches[$menu_name] = $menu->label();
      }
    }
    asort($menu_matches);
    return $menu_matches;
  }

  /**
   * Clears the menu Tree that is kept in state by the class.
   *
   * This is usually used after something has updated a menu item(s).
   */
  protected function clearMenuTreeCache(): void {
    unset($this->menuTree);
    \Drupal::entityTypeManager()->getStorage('menu_link_content')->resetCache();
  }

}
