<?php

namespace Drupal\codit_menu_tools;

/**
 * Interface for MenuManipulator.
 */
interface MenuManipulatorInterface extends MenuManipulatorBaseInterface {

  /**
   * Adds menu item to menu relative to parent and or sibling.
   *
   * @param string $item_title
   *   (required) The title of the menu item.
   * @param string $item_destination
   *   (required) The destination of the menu item.
   * @param string $item_description
   *   The description of the menu item.
   * @param bool $enabled
   *   TRUE to enable or FALSE to disable the menu item.
   * @param string $parent_title
   *   The title of the parent to lookup.
   * @param string $sibling_title
   *   The title of the nearest sibling.
   * @param bool $below
   *   TRUE to place the new menu item below the sibling, FALSE to place above.
   *
   * @return int
   *   The value SAVED_NEW since it is a menu item add.
   */
  public function addMenuItem(
    string $item_title,
    string $item_destination,
    string $item_description = '',
    bool $enabled = TRUE,
    string $parent_title = '',
    string $sibling_title = '',
    bool $below = TRUE
  );

  /**
   * Renames a menu item under an optional parent.
   *
   * Specifying the parent just makes the menu item lookup more precise.
   *
   * @param string $original_title
   *   (required) The title of the menu item.
   * @param string $new_title
   *   (required) The destination of the menu item.
   * @param string $parent_title
   *   The title of the parent to lookup.
   *
   * @return bool
   *   TRUE if renamed, FALSE if not able to find it.
   */
  public function changeMenuItemTitle(
    string $original_title,
    string $new_title,
    string $parent_title = '',
    ): bool;

  /**
   * Moves a menu item from one parent, to an another.
   *
   * @param string $menu_item_title
   *   (required) The title of the menu item to be moved.
   * @param string $original_parent_title
   *   (required) The current parent of the menu item.
   * @param string $new_parent_title
   *   The title of the new parent to move the item to.
   * @param string $new_sibling_title
   *   The sibling to move this item next to.
   * @param bool $below
   *   Whether to put the menu item below the new sibling.
   *
   * @return bool
   *   TRUE if moved, FALSE if not able to find it or unable to find new parent.
   */
  public function changeMenuItemParent(
    string $menu_item_title,
    string $original_parent_title,
    string $new_parent_title,
    string $new_sibling_title = '',
    bool $below = TRUE
    ): bool;

  /**
   * Alters a menu item by renaming, reparenting and or changing sibling.
   *
   * @param string $original_title
   *   (required) The title of the menu item.
   * @param string $new_title
   *   (required) The destination of the menu item.
   * @param string $original_parent_title
   *   (required) The current parent of the menu item.
   * @param string $new_parent_title
   *   (required) The title of the new parent to move the item to.
   * @param string $new_sibling_title
   *   The sibling to move this item next to.
   * @param bool $below
   *   Whether to put the menu item below the new sibling.
   *
   * @return bool
   *   TRUE if moved, FALSE if not able to find it or unable to find new parent.
   */
  public function changeMenuItem(
    string $original_title,
    string $new_title,
    string $original_parent_title,
    string $new_parent_title,
    string $new_sibling_title = '',
    bool $below = TRUE
    ): bool;

  /**
   * Moves a menu item above or below a sibling.
   *
   * @param string $menu_item_title
   *   (required) The title of the menu item.
   * @param string $parent_title
   *   (required) The current parent of the menu item.
   * @param string $new_sibling_title
   *   (required) The sibling to move this item next to.
   * @param bool $below
   *   Whether to put the menu item below the new sibling.
   *
   * @return bool
   *   TRUE if moved, FALSE if not able to find it or unable to find new parent.
   */
  public function changeMenuItemSibling(
    string $menu_item_title,
    string $parent_title,
    string $new_sibling_title = '',
    bool $below = TRUE
    ): bool;

  /**
   * Rearrange items in a menu to match a pattern.
   *
   * @param array $pattern
   *   A pattern of parents and siblings.
   *
   * @codingStandardsIgnoreStart
   * @code
   *  [
   *   'parent1' => [
   *     subset_1_a,
   *     subset_1_b,
   *     subset_1_c,
   *   ]
   *   'parent2' => [
   *     subset_2_a,
   *     subset_2_b,
   *     subset_2_c,
   *   ]
   * ]
   * @endcode
   * @codingStandardsIgnoreEnd
   */
  public function matchPattern(array $pattern): void;

  /**
   * Creates space between all menu item weights in a menu.
   *
   * @return int
   *   The weight of the bottom most item.
   */
  public function menuSeparate(): int;

  /**
   * Move a menu item to a different location in the menu.
   *
   * @param string $item_title
   *   (required) The title of the menu item.
   * @param string $parent_title
   *   The title of the parent to lookup.
   * @param string $sibling_title
   *   The title of the nearest sibling.
   * @param bool $below
   *   TRUE to place the new menu item below the sibling, FALSE to place above.
   * @param bool $last_run
   *   A flag to make on last call to menuSeparate on the way out.
   *
   * @return bool
   *   TRUE if the item existed and was moved, FALSE if could not be found.
   */
  public function moveMenuItem(
    string $item_title,
    string $parent_title = '',
    string $sibling_title = '',
    bool $below = TRUE,
    bool $last_run = TRUE
  ): bool;

}
