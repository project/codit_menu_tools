<?php

namespace Drupal\codit_menu_tools;

use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;

/**
 * Interface for MenuManipulatorBase.
 */
interface MenuManipulatorBaseInterface {

  /**
   * Instantiates the menuTree and assigns the menuName to the class.
   *
   * Can be used to get a new menuTree or retrieve the one already loaded.
   *
   * @param string $menu_name
   *   The machine name of the menu tree to load.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   *   The menu tree that was loaded.
   *
   * @throws \Exception
   *   If the menu does not exist.
   */
  public function getMenuTree(string $menu_name = '');

  /**
   * Gets a menu item by name and optional parent.
   *
   * @param string $menu_item_title
   *   The title of the menu item to lookup.
   * @param string $parent_title
   *   The optional title of the parent.
   *
   * @return \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent|null
   *   A loaded menu item if found, or NULL.
   */
  public function loadMenuItemByNameAndParentName(string $menu_item_title = '', string $parent_title = ''): MenuLinkContent|null;

  /**
   * Gets a menu item by name and optional parent.
   *
   * @param string $menu_item_title
   *   The title of the menu item to lookup.
   * @param string $parent_id
   *   The optional id of the parent to help identify the child.
   *
   * @return \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent|null
   *   A loaded menu item if found, or NULL.
   */
  public function loadMenuItemByNameAndParentId(string $menu_item_title = '', $parent_id = ''): MenuLinkContent|null;

  /**
   * Gets information about menu item by name.
   *
   * @param string $menu_item_title
   *   The title of the menu item to lookup.
   * @param string $parent_id
   *   The optional id of the parent to help identify the child.
   *
   * @return array
   *   An array containing mid, id, weight and parent of the menu item.
   */
  public function findMenuItemDetailsByName(string $menu_item_title = '', $parent_id = ''): array;

  /**
   * Gets an array of all menu machine names in the system.
   *
   * @param string|null $machine_name_partial
   *   Optional string for a partial string match to restrict what is returned.
   *
   * @return array
   *   An array containing 'menu_machine_name' => 'Menu Name' pairs.
   */
  public static function getAllMenuNames(string|null $machine_name_partial = NULL): array;

}
