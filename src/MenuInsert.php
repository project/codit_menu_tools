<?php

namespace Drupal\codit_menu_tools;

/**
 * Class for MenuInsert. This is only a legacy passthrough.
 */
class MenuInsert extends MenuManipulator implements MenuManipulatorInterface {
  // This class only exists for backward compatibility with anyone using the
  // MenuInsert class.
}
