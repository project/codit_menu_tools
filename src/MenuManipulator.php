<?php

namespace Drupal\codit_menu_tools;

use Drupal\menu_link_content\Entity\MenuLinkContent;

/**
 * Class for manipulating menus.
 */
class MenuManipulator extends MenuManipulatorBase implements MenuManipulatorInterface {

  /**
   * {@inheritdoc}
   */
  public function addMenuItem(
    string $item_title,
    string $item_destination,
    string $item_description = '',
    bool $enabled = TRUE,
    string $parent_title = '',
    string $sibling_title = '',
    bool $below = TRUE
  ): int {
    $this->preCheckRequiredItemValues($item_title, $item_destination);
    $this->menuSeparate();
    $parent_details = $this->findMenuItemDetailsByName($parent_title);
    $sibling_details = $this->findMenuItemDetailsByName($sibling_title, $parent_details['id'] ?? '');
    $sibling_weight = $sibling_details['weight'] ?? 0;
    $item_weight = ($below) ? $sibling_weight + 1 : $sibling_weight - 1;
    // Using a redundant backup for parent. Most likely not needed.
    $parent_id = $sibling_details['parent_id'] ?? $parent_details['id'] ?? '';
    $result = MenuLinkContent::create([
      'title' => $item_title,
      'description' => $item_description,
      'link' => ['uri' => $item_destination],
      'menu_name' => $this->menuName,
      'enabled' => $enabled,
      'parent' => $parent_id,
      'weight' => $item_weight,
    ])->save();
    $this->menuSeparate();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function changeMenuItemTitle(string $original_title, string $new_title, string $parent_title = ''): bool {
    return $this->changeMenuItem($original_title, $new_title, $parent_title, $parent_title);
  }

  /**
   * {@inheritdoc}
   */
  public function changeMenuItemParent(
    string $menu_item_title,
    string $original_parent_title,
    string $new_parent_title,
    string $new_sibling_title = '',
    bool $below = TRUE
  ): bool {
    return $this->changeMenuItem($menu_item_title, $menu_item_title, $original_parent_title, $new_parent_title, $new_sibling_title, $below);
  }

  /**
   * {@inheritdoc}
   */
  public function changeMenuItemSibling(
    string $menu_item_title,
    string $parent_title,
    string $new_sibling_title = '',
    bool $below = TRUE
    ): bool {
    return $this->changeMenuItem($menu_item_title, $menu_item_title, $parent_title, $parent_title, $new_sibling_title, $below);
  }

  /**
   * {@inheritdoc}
   */
  public function changeMenuItem(
    string $original_title,
    string $new_title,
    string $original_parent_title,
    string $new_parent_title,
    string $new_sibling_title = '',
    bool $below = TRUE
  ): bool {
    $renamed = FALSE;
    $reparented = FALSE;
    $resiblinged = FALSE;
    $menu_item = $this->loadMenuItemByNameAndParentName($original_title, $original_parent_title);
    $vars = [
      '@title' => $original_title,
      '@new_title' => $new_title,
      '@menu' => $this->menuName,
      '@original_parent' => $original_parent_title,
      '@new_parent' => $new_parent_title,
      ':link' => "/admin/structure/menu/manage/{$this->menuName}",
      '@sibling' => $new_sibling_title,
      '@position' => ($below) ? 'below' : 'above',
    ];
    if ($menu_item) {
      $definition = $menu_item->getPluginDefinition();
      $message = "<a href=\":link\">Menu @menu</a>; ";

      if ($original_title !== $new_title) {
        // The title is changing.
        $definition['title'] = $new_title;
        $renamed = TRUE;
        $message .= "'@title' was renamed to '@new_title'. ";
      }

      if ($original_parent_title !== $new_parent_title) {
        // The parent is changing.
        $new_parent = $this->loadMenuItemByNameAndParentId($new_parent_title);
        if ($new_parent) {
          $definition['parent'] = $new_parent->getPluginID();
          $reparented = TRUE;
          $message .= "'@new_title' was moved from parent '@original_parent' to '@new_parent'.";
        }
        else {
          // The new parent was not found.
          $no_parent_msg = "Menu @menu; '@title' was not moved to '@new_parent'. </br>";
          $no_parent_msg .= "Reason: The new parent '@new_parent' was not found. </br>";
          $no_parent_msg .= "Check menu <a href=\":link\">@menu</a> to see if it needs a menu item named '@new_parent' added or re-named.";
          $this->logger->notice($no_parent_msg, $vars);
          return FALSE;
        }
      }

      if ($new_sibling_title) {
        // This is going to a new sibling so use its weight..
        $sibling_menu_item = $this->loadMenuItemByNameAndParentName($new_sibling_title, $new_parent_title);
        $weight = (!empty($sibling_menu_item)) ? $sibling_menu_item->getWeight() : NULL;
        if (is_null($weight)) {
          // We are flying blind because a sibling was not available, bail out.
          $message = "Menu @menu; '@title'was not moved. </br>";
          $message .= "Reason: Sibling menu item '@sibling' under parent '@new_parent' was not found. </br>";
          $message .= "Check menu <a href=\":link\">@menu</a> to see if it needs a menu item named '@sibling' under parent '@new_parent' and move '@title' manually.";
          $this->logger->notice($message, $vars);
          return FALSE;
        }
        else {
          // The sibling exists, so use it.
          $definition['weight'] = ($below) ? $weight + 1 : $weight - 1;
          $message .= "'@new_title' was placed @position '@sibling'. </br>";
          $resiblinged = TRUE;
        }
      }

      if ($renamed || $reparented || $resiblinged) {
        $this->menuSeparate();
        $this->menuManager->updateDefinition($menu_item->getPluginId(), $definition);
        $this->clearMenuTreeCache();
        $this->logger->info($message, $vars);
      }
    }
    else {
      // Nothing to change the title of.  Log a message.
      if ($original_title !== $new_title) {
        // This was an intended title change that failed.
        $message = "Menu @menu; '@title' was not renamed to '@new_title'. </br>";
        $message .= "Reason: The menu item '@title' under parent '@original_parent' was not found. </br>";
      }
      else {
        // This was an intended parent change that failed.
        $message = "Menu @menu; '@title' was not moved from '@original_parent to '@new_parent' because it was not found. </br>";
      }
      $message .= "Check menu <a href=\":link\">@menu</a> to see if it needs a menu item named '@title' under parent '@original_parent'.";
      $this->logger->notice($message, $vars);
    }
    return $renamed || $reparented || $resiblinged;
  }

  /**
   * {@inheritdoc}
   */
  public function matchPattern(array $pattern, string $parent = ''): void {
    $max_loops = count($pattern);
    $keys = array_keys($pattern);
    $i = 0;
    foreach ($pattern as $index => $item) {
      if (is_array($item)) {
        // This is a subtree and the index is the parent.
        $this->matchPattern($item, $index);
        // The index is actually an item to act on.
        $item = $index;
        $index = $i;
      }
      if (empty($index)) {
        // This is the first item in the list and we don't know where to move
        // it, so leave it where it is so it can be the reference for the next.
        $i++;
      }
      else {
        // We can move things.
        $previous_item_key = $keys[$i - 1];
        $previous_sibling = ($index) ? $pattern[$previous_item_key] : '';
        $last_run = (++$i === $max_loops) ? TRUE : FALSE;
        // No previous sibling means we are at the top of the list so needs to
        // go above the entries.
        // Most likely this test is redundant to the first item check above.
        $below = (empty($previous_sibling)) ? FALSE : TRUE;
        $this->moveMenuItem($item, $parent, $previous_sibling, $below, $last_run);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function menuSeparate(): int {
    $this->clearMenuTreeCache();
    $tree = $this->getMenuTree();
    $highest_weight = $this->separateTreeBranch($tree);
    $this->menuManager->rebuild();
    $this->clearMenuTreeCache();

    return $highest_weight;
  }

  /**
   * {@inheritdoc}
   */
  public function moveMenuItem(
    string $item_title,
    string $parent_title = '',
    string $sibling_title = '',
    bool $below = TRUE,
    bool $last_run = TRUE
    ): bool {
    // @todo The moveMenuItem() is a subset of changeMenuItem().  moveMenuItem
    // should be refactored to use changeMenuItem().
    $moved = FALSE;
    $highest_weight = $this->menuSeparate();
    if (empty($sibling_title)) {
      // There is no sibling so move it either to top or the bottom of menu.
      $weight = ($below) ? $highest_weight + 2 : -2;
    }
    else {
      // There is a sibling so use its weight.
      $sibling_menu_item = $this->loadMenuItemByNameAndParentName($sibling_title, $parent_title);
      $weight = ($sibling_menu_item) ? $sibling_menu_item->getWeight() : NULL;
      if (is_null($weight)) {
        // We are flying blind because a sibling was not available so bail out.
        $vars = [
          '@title' => $item_title,
          '@menu' => $this->menuName,
          '@parent' => $parent_title,
          '@sibling' => $sibling_title,
          ':link' => "/admin/structure/menu/manage/{$this->menuName}",
        ];
        $message = "Menu @menu; '@title'was not moved. </br>";
        $message .= "Reason: Sibling menu item '@sibling' under parent '@parent' was not found. </br>";
        $message .= "Check menu <a href=\":link\">@menu</a> to see if it needs a menu item named '@sibling' under parent '@parent' and move '@title' manually.";
        $this->logger->notice($message, $vars);
        return $moved;
      }
      $weight = ($below) ? $weight + 1 : $weight - 1;
    }

    // Now move the item.
    $menu_item = $this->loadMenuItemByNameAndParentName($item_title, $parent_title);
    if (empty($menu_item)) {
      // Could not find it.
      $vars = [
        '@title' => $item_title,
        '@menu' => $this->menuName,
        '@parent' => $parent_title,
        ':link' => "/admin/structure/menu/manage/{$this->menuName}",
      ];
      $message = "Menu @menu; '@title'was not moved. </br>";
      $message .= "Reason: The menu item '@title' under parent '@parent' was not found. </br>";
      $message .= "Check menu <a href=\":link\">@menu</a> to see if it needs a menu item named '@title' under parent '@parent' and move '@title' manually.";
      $this->logger->notice($message, $vars);
    }
    elseif (($menu_item->getWeight() !== $weight)) {
      // There is a change of weight, so save it.
      $definition = $menu_item->getPluginDefinition();
      $definition['weight'] = $weight;
      $this->menuManager->updateDefinition($menu_item->getPluginId(), $definition);
      $moved = TRUE;
      $this->clearMenuTreeCache();
    }
    // Only separate the menu items on the last run, to keep from stacking
    // the pre and post menuSeparate().
    if ($last_run) {
      $this->menuSeparate();
    }
    return $moved;
  }

}
