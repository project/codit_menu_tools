
Codit Menu Tools offers nothing by itself. It only creates a class that can be
used in hook_updates or other scripts to manipulate menus such as

* Adding a menu item in a certain spot.
* Organizing the order of existing menu items to match. (coming soon)

## Features
Use cases for this module are for developers to more easily and smoothly manage
menu items within a menu.

Available actions through code:

  - Add menu items in a specific location relative to a parent and sibling -
    **addMenuItem()**
  - Change the title of a menu item - **changeMenuItemTitle()**
  - Change a menu item's parent - **changeMenuItemParent()**
  - Change a menu item's nearest sibling - **changeMenuItemSibling()**
    Duplicate of **moveMenuItem()**
  - Rename, reparent and or re-sibling a menu item - **changeMenuItem()**
  - Create space in the weights of all menu items - **menuSeparate()**
  - Re-arrange menu items to match a pattern - **matchPattern()**
  - Find and retrieve menu items based on parent and title -
    **loadMenuItemByNameAndParentName()**
  - Get a list or filtered list of all menus in the system -
    **getAllMenuNames()**

## Post-Installation
There is no configuration. Install it, see examples for how to do things in
update hooks (hook_update_n), post update hooks, post deploy hooks, or scripts.

## Additional Requirements
Drupal core's menu_link_content is the only thing needed.

## Recommended modules/libraries
None. This module does not build upon or add features to any other module. It
simply allows developers to not have to create custom menu item manipulation
code.

## Similar projects
There is currently nothing similar.

## Examples

### Adding a node to a menu.

  This is useful after loading or creating a node.  It allows precise placement
  of any new menu item within the menu.

```php
    use Drupal\codit_menu_tools\MenuManipulator;

    $node->save();

    $menuManipulator = new MenuManipulator('MENU_MACHINE_NAME');
    $menu_item_title = "Some title";
    $$menu_item_destination = "entity:node/{$node->id()}";
    $menu_item_description = 'I am the description';
    $menu_item_enabled = TRUE;
    $parent_title = 'News and Events';
    $adjacent_sibling = 'Stories';
    $place_below = TRUE;
    // This will place the enabled menu item under the parent of "News and
    // Events" and just beneath the sibling "Stories".
    $menuManipulator->addMenuItem(
      $menu_item_title,
      $menu_item_destination,
      $menu_item_description,
      $menu_item_enabled,
      $parent_title,
      $adjacent_sibling,
      $place_below
    );
```

### Changing the title of a menu item.

This allows an easy way of changing the title (name) of a menu item.
The parent is only useful for being more specific/certain about modifying the
menu item you want to.

```php
  use Drupal\codit_menu_tools\MenuManipulator;

  $menuManipulator = new MenuManipulator('MENU_MACHINE_NAME');
  $success = $menuManipulator->changeMenuItemTitle($title, $new_title, $parent_title);
```

### Changing the parent of a menu item.

This allows an easy way of changing the parent of a menu item.

```php
  use Drupal\codit_menu_tools\MenuManipulator;

  $menuManipulator = new MenuManipulator('MENU_MACHINE_NAME');
  $success = $menuManipulator->changeMenuItemParent($title, $original_parent_title, $new_parent_title);
```

### Creating space between the weights of all menu items in a menu.

  This creates space between menu item weights of all items in a menu, while
  preserving the order and hierarchy.  No two adjacent menu items will have the
  same weight, and there will be a gap large enough to fit another menu item
  between any two existing items.

```php
    use Drupal\codit_menu_tools\MenuManipulator;

    $menuManipulator = new MenuManipulator('MENU_MACHINE_NAME');
    $highest_weight_used = $menuManipulator->menuSeparate();
```

### Move existing menu items to new to different weights under the same parent.

```php
    use Drupal\codit_menu_tools\MenuManipulator;

    $menuManipulator = new MenuManipulator('MENU_MACHINE_NAME');
    $menuManipulator->moveMenuItem(
      $item_title = "the menu item title",
      $parent_title = "the title of the parent to look under",
      $sibling_title = "The title of the sibling to move it adjacent to",
      $below = TRUE, // Place it just above or just below the sibling.
      $last_run = TRUE, // If moving a bunch of items, set this to FALSE, except
      // for the last thing you move, then set it to TRUE.
  );
```

### Move a bunch of menu items to align with a pattern.

  This moves menu items up or down under the same parent so that the order
  reflects what is in the pattern.

  This can NOT be used to change a menu item's parent.

  The top most item of the tree or subtree will not be moved, it is used as the
  starting place for anything that follows.

```php
  use Drupal\codit_menu_tools\MenuManipulator;

  // A simple pattern affecting items without specifying a parent.
  $pattern = [
    'item 1',
    'item 2',
    'item 3',
    'item 4',
  ];

  // A more complex pattern involving multiple parents.  This would rearrange
  // the subtrees (branches) AND the parents.
  $pattern = [
    'parent1' => [
      'subtree_1_a',
      'subtree_1_b',
      'subtree_1_c',
    ]
    'parent2' => [
       'subtree_2_a',
       'subtree_2_b',
       'subtree_2_c',
    ]
  ];

  $menuManipulator = new MenuManipulator('MENU_MACHINE_NAME');
  $menuManipulator->matchPattern($pattern);
```
